﻿using AhegaoFurryDiscordBot.Core.Abstraction;
using AhegaoFurryDiscordBot.Core.DTO;
using Microsoft.Extensions.Hosting;

namespace AhegaoFurryDiscordBot.Infrastructure.Services.BackgroundTaskQueue;

public sealed class QueuedTaskChangeMessageHostedService : BackgroundService
{
    private readonly IChangeMessageBackgroundTaskQueue _taskQueue;

    public QueuedTaskChangeMessageHostedService(
        IChangeMessageBackgroundTaskQueue changeMessageBackgroundTaskQueue)
    {
        _taskQueue = changeMessageBackgroundTaskQueue;
    }
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                var workItem = await _taskQueue.DequeueAsync(stoppingToken);

                await ProcessTick(workItem, stoppingToken);
            }
            catch (OperationCanceledException) { }
        }
    }

    private async Task ProcessTick(ChangeMessageBackgroundTaskQueueStore workItem, CancellationToken stoppingToken)
    {
        if (workItem.FinishedAt > DateTime.Now)
        {
            await _taskQueue.EnqueueAsync(workItem);
            return;
        }
        
        var iterationResult = await workItem.Func(stoppingToken);
        await Task.Delay(iterationResult.MillisecondsMessage, stoppingToken);
    }
}