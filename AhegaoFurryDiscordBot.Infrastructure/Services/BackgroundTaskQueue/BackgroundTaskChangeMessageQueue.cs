﻿using System.Threading.Channels;
using AhegaoFurryDiscordBot.Core.Abstraction;
using AhegaoFurryDiscordBot.Core.DTO;
using MapsterMapper;

namespace AhegaoFurryDiscordBot.Infrastructure.Services.BackgroundTaskQueue;

public class BackgroundTaskChangeMessageQueue : IChangeMessageBackgroundTaskQueue
{
    private readonly Channel<ChangeMessageBackgroundTaskQueueStore> _queue;
    
    public BackgroundTaskChangeMessageQueue(IMapper mapper)
    {
        BoundedChannelOptions options = new(100)
        {
            FullMode = BoundedChannelFullMode.Wait
        };

        _queue = Channel.CreateBounded<ChangeMessageBackgroundTaskQueueStore>(options);
    }
    
    public async ValueTask EnqueueAsync(ChangeMessageBackgroundTaskQueueStore workItem)
    {
        await _queue.Writer.WriteAsync(workItem);
    }

    public async ValueTask<ChangeMessageBackgroundTaskQueueStore> DequeueAsync(CancellationToken cancellationToken)
    {
        return await _queue.Reader.ReadAsync(cancellationToken);
    }
}