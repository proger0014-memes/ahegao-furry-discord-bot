﻿namespace AhegaoFurryDiscordBot.Infrastructure.Configurations;

#nullable disable
public class BotConfig
{
    public string Token { get; set; }
}