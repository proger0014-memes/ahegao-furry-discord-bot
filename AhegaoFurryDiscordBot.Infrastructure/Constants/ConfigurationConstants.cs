﻿namespace AhegaoFurryDiscordBot.Infrastructure.Constants;

public static class ConfigurationConstants
{
    public const string Postgres = nameof(Postgres);
    public const string EnvironmentsPrefix = "BOT_";
}