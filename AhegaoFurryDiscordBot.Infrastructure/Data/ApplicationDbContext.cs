﻿using System.Reflection;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using Microsoft.EntityFrameworkCore;

namespace AhegaoFurryDiscordBot.Infrastructure.Data;

public class ApplicationDbContext : DbContext
{
    public DbSet<UserEntity>? Users { get; set; }
    public DbSet<QuizEntity>? Quizzes { get; set; }
    public DbSet<QuizCommandEntity>? QuizCommands { get; set; }
    public DbSet<QuizInteractionEntity>? QuizInteractions { get; set; }
    public DbSet<QuizValueEntity>? QuizValues { get; set; }
    
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetCallingAssembly());
        
        base.OnModelCreating(modelBuilder);
    }
}