﻿using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhegaoFurryDiscordBot.Infrastructure.Data.Entities;

public class UserEntity : BaseEntity
{
    public ICollection<QuizEntity> Quizzes { get; set; }
    public ICollection<QuizValueEntity> QuizValues { get; set; }
}

public class UserEntityConfiguration : IEntityTypeConfiguration<UserEntity>
{
    public void Configure(EntityTypeBuilder<UserEntity> builder)
    {
        builder.HasKey(u => u.Id);

        builder.HasMany(u => u.QuizValues)
            .WithOne(qv => qv.UserReference)
            .HasForeignKey(qv => qv.UserId)
            .HasPrincipalKey(u => u.Id);

        builder.HasMany(u => u.Quizzes)
            .WithOne(q => q.UserReference)
            .HasForeignKey(q => q.UserIdBy)
            .HasPrincipalKey(u => u.Id);
    }
}