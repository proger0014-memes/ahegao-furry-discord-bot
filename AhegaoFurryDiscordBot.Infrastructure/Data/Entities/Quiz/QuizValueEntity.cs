﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;

public class QuizValueEntity
{
    public long QuizId { get; set; }
    public string QuizCommand { get; set; }
    public string QuizInteraction { get; set; }
    public long UserId { get; set; }

    public QuizEntity QuizReference { get; } = null!;
    public QuizCommandEntity QuizCommandReference { get; } = null!;
    public QuizInteractionEntity QuizInteractionReference { get; } = null!;
    public UserEntity UserReference { get; } = null!;
}

public class QuizValueEntityConfiguration : IEntityTypeConfiguration<QuizValueEntity>
{
    public void Configure(EntityTypeBuilder<QuizValueEntity> builder)
    {
        builder.HasNoKey();
    }
}