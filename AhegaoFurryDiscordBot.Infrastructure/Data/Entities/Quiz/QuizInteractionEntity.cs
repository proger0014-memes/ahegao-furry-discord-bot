﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;

public class QuizInteractionEntity
{
    public string Interaction { get; set; }
    
    public ICollection<QuizValueEntity> QuizValues { get; set; }
}

public class QuizInteractionEntityConfiguration : IEntityTypeConfiguration<QuizInteractionEntity>
{
    public void Configure(EntityTypeBuilder<QuizInteractionEntity> builder)
    {
        builder.HasKey(qi => qi.Interaction);

        builder.HasMany(qi => qi.QuizValues)
            .WithOne(qv => qv.QuizInteractionReference)
            .HasForeignKey(qv => qv.QuizInteraction)
            .HasPrincipalKey(qi => qi.Interaction);
    }
}