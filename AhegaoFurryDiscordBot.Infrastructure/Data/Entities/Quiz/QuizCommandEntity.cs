﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;

public class QuizCommandEntity
{
    public string Command { get; set; }
    
    public ICollection<QuizValueEntity> QuizValues { get; set; }
}

public class QuizCommandEntityConfiguration : IEntityTypeConfiguration<QuizCommandEntity>
{
    public void Configure(EntityTypeBuilder<QuizCommandEntity> builder)
    {
        builder.HasKey(qc => qc.Command);
        
        builder.HasMany(qc => qc.QuizValues)
            .WithOne(qv => qv.QuizCommandReference)
            .HasForeignKey(qv => qv.QuizCommand)
            .HasPrincipalKey(qc => qc.Command);
    }
}