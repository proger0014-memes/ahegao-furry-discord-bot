﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;

public class QuizEntity : BaseEntity
{
    public long UserIdBy { get; set; }
    public DateTime AddedAt { get; set; }
    public long ChannelId { get; set; }

    public UserEntity UserReference { get; } = null!;
    public ICollection<QuizValueEntity> QuizValues { get; } = new List<QuizValueEntity>();
}

public class QuizEntityConfiguration : IEntityTypeConfiguration<QuizEntity>
{
    public void Configure(EntityTypeBuilder<QuizEntity> builder)
    {
        builder.HasKey(q => q.Id);

        builder.HasMany(q => q.QuizValues)
            .WithOne(qv => qv.QuizReference)
            .HasForeignKey(qv => qv.QuizId)
            .HasPrincipalKey(q => q.Id);
    }
}