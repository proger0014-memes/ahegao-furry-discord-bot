﻿using AhegaoFurryDiscordBot.Core.Models.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using Mapster;

namespace AhegaoFurryDiscordBot.Infrastructure.Mappings;

public class QuizMappings : IRegister
{
    public void Register(TypeAdapterConfig config)
    {
        config.ForType<Quiz, QuizEntity>()
            .TwoWays();
        config.ForType<QuizValue, QuizValueEntity>()
            .TwoWays();
        config.ForType<QuizCommand, QuizCommandEntity>()
            .TwoWays();
        config.ForType<QuizInteraction, QuizInteractionEntity>()
            .TwoWays();

        config.ForType<List<QuizValueEntity>, IEnumerable<QuizValue>>();
    }
}