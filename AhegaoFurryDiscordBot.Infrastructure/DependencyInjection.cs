﻿using AhegaoFurryDiscordBot.Core.Abstraction;
using AhegaoFurryDiscordBot.Core.Abstraction.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Configurations;
using AhegaoFurryDiscordBot.Infrastructure.Constants;
using AhegaoFurryDiscordBot.Infrastructure.Data;
using AhegaoFurryDiscordBot.Infrastructure.Repositories;
using AhegaoFurryDiscordBot.Infrastructure.Services.BackgroundTaskQueue;
using Mapster;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AhegaoFurryDiscordBot.Infrastructure;

public static class DependencyInjection
{
    public static void RegisterInfrastructure(this IServiceCollection services, IConfigurationManager configuration)
    {
        services
            .AddMapster()
            .AddBackgroundService()
            .AddSettings(configuration)
            .AddDatabase(configuration);
    }

    private static IServiceCollection AddRepositories(this IServiceCollection serviceCollection)
    {
        serviceCollection
            .AddSingleton<IQuizRepository, QuizRepository>()
            .AddSingleton<IQuizCommandRepository, QuizCommandRepository>()
            .AddSingleton<IQuizInteractionRepository, QuizInteractionRepository>()
            .AddSingleton<IQuizValueRepository, QuizValueRepository>();

        return serviceCollection;
    }
    
    private static IServiceCollection AddSettings(this IServiceCollection serviceCollection, IConfigurationManager configuration)
    {
        configuration.AddEnvironmentVariables(ConfigurationConstants.EnvironmentsPrefix);

        serviceCollection.Configure<BotConfig>(configuration.GetSection(nameof(BotConfig)));

        return serviceCollection;
    }

    private static IServiceCollection AddDatabase(this IServiceCollection serviceCollection, IConfiguration configuration)
    {
        serviceCollection.AddDbContext<ApplicationDbContext>(builder =>
            builder.UseNpgsql(configuration.GetConnectionString(ConfigurationConstants.Postgres)));

        return serviceCollection;
    }

    private static IServiceCollection AddBackgroundService(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<IChangeMessageBackgroundTaskQueue, BackgroundTaskChangeMessageQueue>();
        serviceCollection.AddHostedService<QueuedTaskChangeMessageHostedService>();

        return serviceCollection;
    }

    private static IServiceCollection AddMapster(this IServiceCollection serviceCollection)
    {
        var config = new TypeAdapterConfig();
        config.Scan(AppDomain.CurrentDomain.GetAssemblies());

        serviceCollection.AddSingleton(config);
        serviceCollection.AddSingleton<IMapper, ServiceMapper>();

        return serviceCollection;
    }
}