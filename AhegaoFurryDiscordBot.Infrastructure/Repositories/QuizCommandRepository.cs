﻿using AhegaoFurryDiscordBot.Core.Abstraction.Quiz;
using AhegaoFurryDiscordBot.Core.Models.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Data;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace AhegaoFurryDiscordBot.Infrastructure.Repositories;

public class QuizCommandRepository : IQuizCommandRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public QuizCommandRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }
    
    public async Task Insert(QuizCommand newQuizCommand)
    {
        var newQuizCommandEntity = _mapper.Map<QuizCommandEntity>(newQuizCommand);

        await _dbContext.QuizCommands!.AddAsync(newQuizCommandEntity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<bool> IsExists(QuizCommand existsQuizCommand)
    {
        var existsQuizCommandEntity = await _dbContext.QuizCommands!
            .Where(qc => qc.Command == existsQuizCommand.Command)
            .FirstOrDefaultAsync();

        return existsQuizCommandEntity is not null;
    }
}