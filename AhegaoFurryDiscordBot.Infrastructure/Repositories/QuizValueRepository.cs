﻿using AhegaoFurryDiscordBot.Core.Abstraction.Quiz;
using AhegaoFurryDiscordBot.Core.Models.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Data;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace AhegaoFurryDiscordBot.Infrastructure.Repositories;

public class QuizValueRepository : IQuizValueRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public QuizValueRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }
    
    public async Task Insert(QuizValue newQuizValue)
    {
        var newQuizValueEntity = _mapper.Map<QuizValueEntity>(newQuizValue);

        await _dbContext.QuizValues!.AddAsync(newQuizValueEntity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<IEnumerable<QuizValue>> GetAllByQuizId(long quizId)
    {
        var existsQuizValues = await _dbContext.QuizValues!
            .Where(qv => qv.QuizId == quizId)
            .ToListAsync();

        return _mapper.Map<IEnumerable<QuizValue>>(existsQuizValues);
    }
}