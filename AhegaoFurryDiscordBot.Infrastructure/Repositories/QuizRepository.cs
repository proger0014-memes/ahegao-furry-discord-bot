﻿using AhegaoFurryDiscordBot.Core.Abstraction.Quiz;
using AhegaoFurryDiscordBot.Core.Models.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Data;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace AhegaoFurryDiscordBot.Infrastructure.Repositories;

public class QuizRepository : IQuizRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public QuizRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }
    
    public async Task Insert(Quiz newQuiz)
    {
        var newQuizEntity = _mapper.Map<QuizEntity>(newQuiz);

        await _dbContext.Quizzes!.AddAsync(newQuizEntity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<Quiz?> GetById(long id)
    {
        var existsQuiz = await _dbContext.Quizzes!.Where(q => q.Id == id).FirstOrDefaultAsync();

        if (existsQuiz is null) return null;
        
        return _mapper.Map<Quiz>(existsQuiz);
    }
}