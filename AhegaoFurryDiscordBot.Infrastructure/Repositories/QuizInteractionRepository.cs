﻿using AhegaoFurryDiscordBot.Core.Abstraction.Quiz;
using AhegaoFurryDiscordBot.Core.Models.Quiz;
using AhegaoFurryDiscordBot.Infrastructure.Data;
using AhegaoFurryDiscordBot.Infrastructure.Data.Entities.Quiz;
using MapsterMapper;
using Microsoft.EntityFrameworkCore;

namespace AhegaoFurryDiscordBot.Infrastructure.Repositories;

public class QuizInteractionRepository : IQuizInteractionRepository
{
    private readonly ApplicationDbContext _dbContext;
    private readonly IMapper _mapper;

    public QuizInteractionRepository(ApplicationDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext;
        _mapper = mapper;
    }
    
    public async Task Insert(QuizInteraction newQuizInteraction)
    {
        var newQuizInteractionEntity = _mapper.Map<QuizInteractionEntity>(newQuizInteraction);

        await _dbContext.QuizInteractions!.AddAsync(newQuizInteractionEntity);

        await _dbContext.SaveChangesAsync();
    }

    public async Task<bool> IsExists(QuizInteraction existsQuizInteraction)
    {
        var existsQuizInteractionEntity = await _dbContext.QuizInteractions!
            .Where(qi => qi.Interaction == existsQuizInteraction.Interaction)
            .FirstOrDefaultAsync();

        return existsQuizInteractionEntity is not null;
    }
}