﻿using System.Diagnostics;

namespace AhegaoFurryDiscordBot.Core.DTO;

public record ChangeMessageBackgroundTaskQueueResult(int MillisecondsMessage);
public record ChangeMessageBackgroundTaskQueueStore(
    DateTime FinishedAt,
    Func<CancellationToken, ValueTask<ChangeMessageBackgroundTaskQueueResult>> Func);