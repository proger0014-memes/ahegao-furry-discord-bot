﻿namespace AhegaoFurryDiscordBot.Core.Models.Quiz;

public class QuizValue
{
    public long QuizId { get; set; }
    public string QuizCommand { get; set; }
    public string QuizInteraction { get; set; }
    public long UserId { get; set; }
}