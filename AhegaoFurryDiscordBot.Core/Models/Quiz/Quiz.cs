﻿namespace AhegaoFurryDiscordBot.Core.Models.Quiz;

public class Quiz : BaseModel
{
    public long UserIdBy { get; set; }
    public DateTime AddedAt { get; set; }
    public long ChannelId { get; set; }
}