﻿namespace AhegaoFurryDiscordBot.Core.Abstraction.User;

public interface IUserRepository
{
    Task Insert(Models.User newUser);
    Task<bool> IsExists(Models.User existsUser);
}