﻿using CSharpFunctionalExtensions;

namespace AhegaoFurryDiscordBot.Core.Abstraction.User;

public interface IUserService
{
    Task<Result> Add(Models.User newUser);
    Task<Result<bool>> IsExists(Models.User existsUser);
}