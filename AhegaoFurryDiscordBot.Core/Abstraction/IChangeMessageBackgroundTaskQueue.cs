﻿using AhegaoFurryDiscordBot.Core.DTO;

namespace AhegaoFurryDiscordBot.Core.Abstraction;

public interface IChangeMessageBackgroundTaskQueue
{
    ValueTask EnqueueAsync(ChangeMessageBackgroundTaskQueueStore workItem);
    ValueTask<ChangeMessageBackgroundTaskQueueStore> DequeueAsync(CancellationToken cancellationToken);
}
