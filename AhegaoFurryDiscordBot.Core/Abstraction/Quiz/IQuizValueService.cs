﻿using CSharpFunctionalExtensions;

namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizValueService
{
    Task<Result> Add(Models.Quiz.QuizValue newQuizValue);
    Task<Result<IEnumerable<Models.Quiz.QuizValue>>> GetAllByQuizId(long quizId);
}