﻿using CSharpFunctionalExtensions;

namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizCommandService
{
    Task<Result> Add(Models.Quiz.QuizCommand newQuizCommand);
    Task<Result<bool>> IsExists(Models.Quiz.QuizCommand existsQuizCommand);
}