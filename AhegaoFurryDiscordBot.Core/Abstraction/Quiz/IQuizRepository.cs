﻿namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizRepository
{
    Task Insert(Models.Quiz.Quiz newQuiz);
    Task<Models.Quiz.Quiz?> GetById(long id);
}