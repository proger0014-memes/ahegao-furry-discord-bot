﻿namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizValueRepository
{
    Task Insert(Models.Quiz.QuizValue newQuizValue);
    Task<IEnumerable<Models.Quiz.QuizValue>> GetAllByQuizId(long quizId);
}