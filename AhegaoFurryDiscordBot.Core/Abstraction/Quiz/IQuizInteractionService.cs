﻿using CSharpFunctionalExtensions;

namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizInteractionService
{
    Task<Result> Add(Models.Quiz.QuizInteraction newQuizInteraction);
    Task<Result<bool>> IsExists(Models.Quiz.QuizInteraction existsQuizInteraction);
}