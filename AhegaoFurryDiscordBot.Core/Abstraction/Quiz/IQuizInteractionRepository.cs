﻿namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizInteractionRepository
{
    Task Insert(Models.Quiz.QuizInteraction newQuizInteraction);
    Task<bool> IsExists(Models.Quiz.QuizInteraction existsQuizInteraction);
}