﻿using CSharpFunctionalExtensions;

namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizService
{
    Task<Result> Add(Models.Quiz.Quiz newQuiz);
    Task<Result<Models.Quiz.Quiz>> GetById(long id);
}