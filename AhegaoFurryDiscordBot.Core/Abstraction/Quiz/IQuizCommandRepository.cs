﻿namespace AhegaoFurryDiscordBot.Core.Abstraction.Quiz;

public interface IQuizCommandRepository
{
    Task Insert(Models.Quiz.QuizCommand newQuizCommand);
    Task<bool> IsExists(Models.Quiz.QuizCommand existsQuizCommand);
}