﻿using AhegaoFurryDiscordBot.CLI.Constants.InteractionsConstants;
using AhegaoFurryDiscordBot.CLI.Views;
using AhegaoFurryDiscordBot.Core.Abstraction;
using AhegaoFurryDiscordBot.Core.DTO;
using Discord.Interactions;

namespace AhegaoFurryDiscordBot.CLI.Interactions.Quiz;

[Group(QuizConstants.QuizGroupName, QuizConstants.QuizGroupDescription)]
public class QuizCommands : InteractionModuleBase<SocketInteractionContext>
{
    private readonly IChangeMessageBackgroundTaskQueue _changeMessageBackgroundTaskQueue;

    public QuizCommands(IChangeMessageBackgroundTaskQueue changeMessageBackgroundTaskQueue)
    {
        _changeMessageBackgroundTaskQueue = changeMessageBackgroundTaskQueue;
    }
    
    private async Task CreateYesNoHelper(string message)
    {
        var buttons = QuizViews.LohComponent();

        var startedTime = DateTime.Now;
        var finishedTime = startedTime.AddSeconds(QuizConstants.SecondsTimeout);
            
        var embed = QuizViews.LohComponentContent(message, startedTime, finishedTime);
        var finishedEmbed = QuizViews.LohComponentContent(message, startedTime, finishedTime, true);

        var replyResult = await ReplyAsync(components: buttons, embed: embed);

        async ValueTask<ChangeMessageBackgroundTaskQueueResult> FinishedFunc(CancellationToken stoppingToken)
        {
            await replyResult.ModifyAsync(rr => rr.Embed = finishedEmbed);
                
            return new ChangeMessageBackgroundTaskQueueResult(replyResult.EditedTimestamp!.Value.Millisecond);
        }

        await _changeMessageBackgroundTaskQueue.EnqueueAsync(
            new ChangeMessageBackgroundTaskQueueStore(finishedTime, FinishedFunc));
    }
    
    [SlashCommand(QuizConstants.LohCommandName, QuizConstants.LohCommandDescription)]
    public async Task CreateYesNoLoh([Summary("user", "Кто лох")] string user)
    {
        await CreateYesNoHelper($"{user} лох?");

        await RespondAsync();
    }
}