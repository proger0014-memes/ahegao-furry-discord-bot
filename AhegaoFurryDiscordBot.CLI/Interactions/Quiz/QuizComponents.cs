﻿using AhegaoFurryDiscordBot.CLI.Constants.InteractionsConstants;
using Discord;
using Discord.Interactions;
using Discord.WebSocket;

namespace AhegaoFurryDiscordBot.CLI.Interactions.Quiz;

public class QuizComponents : InteractionModuleBase<SocketInteractionContext>
{
    [ComponentInteraction(QuizConstants.LohComponentBtnYes)]
    public async Task HandleYesNoComponentBtnYes()
    {
        await ((SocketMessageComponent)Context.Interaction).UpdateAsync(f =>
        {
            f.Components = new ComponentBuilder()
                .WithButton("Да", QuizConstants.LohComponentBtnYes, ButtonStyle.Danger).Build();
        });

        await RespondAsync();
    }
}