﻿using System.Reflection;
using AhegaoFurryDiscordBot.Infrastructure.Configurations;
using Discord;
using Discord.Interactions;
using Discord.Interactions.Builders;
using Discord.WebSocket;
using Microsoft.Extensions.Hosting;

namespace AhegaoFurryDiscordBot.CLI;

public class BotApplication
{
    private readonly DiscordSocketClient _client;
    private readonly InteractionService _interactionService;

    private readonly IHost _host;

    private readonly BotConfig _botConfig;

    public BotApplication(
        IHost host,
        BotConfig botConfig,
        DiscordSocketClient client,
        InteractionService interactionService)
    {
        _host = host;
        _botConfig = botConfig;
        _client = client;
        _interactionService = interactionService;
    }

    private async Task AddInteractions()
    {
        await _interactionService.AddModulesAsync(Assembly.GetEntryAssembly(), _host.Services);

        _client.InteractionCreated += async (arg) =>
        {
            try
            {
                var ctx = new SocketInteractionContext(_client, arg);
                await _interactionService.ExecuteCommandAsync(ctx, _host.Services);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        };

        _client.Ready += async () =>
        {
            // добавляя это, регистрирует SlashCommandAttribute
            await _interactionService.RegisterCommandsGloballyAsync();
        };
    }
    
    public Task Run()
    {
        _client.LoginAsync(TokenType.Bot, _botConfig.Token).GetAwaiter().GetResult();
        _client.StartAsync().GetAwaiter().GetResult();
        
        AddInteractions().GetAwaiter().GetResult();

        _host.RunAsync().GetAwaiter().GetResult();
        
        return Task.CompletedTask;
    }
}