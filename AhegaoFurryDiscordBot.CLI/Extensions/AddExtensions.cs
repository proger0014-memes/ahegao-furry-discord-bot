﻿using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

namespace AhegaoFurryDiscordBot.CLI.Extensions;

public static class AddExtensions
{
    public static void AddDiscordNet(this IServiceCollection services)
    {
        services.AddSingleton<DiscordSocketClient>();
        services.AddSingleton<InteractionService>(sp => 
            new InteractionService(sp.GetRequiredService<DiscordSocketClient>()));
    }
}