﻿using AhegaoFurryDiscordBot.CLI.Builders;
using AhegaoFurryDiscordBot.CLI.Extensions;
using AhegaoFurryDiscordBot.Infrastructure;

var builder = BotBuilder.Create();

builder.Services.RegisterInfrastructure(builder.Configuration);
builder.Services.AddDiscordNet();

var bot = builder.Build();

bot.Run();