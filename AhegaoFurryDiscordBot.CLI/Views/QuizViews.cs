﻿using AhegaoFurryDiscordBot.CLI.Constants.InteractionsConstants;
using AhegaoFurryDiscordBot.CLI.Utils;
using Discord;

namespace AhegaoFurryDiscordBot.CLI.Views;

public static class QuizViews
{
    public static MessageComponent LohComponent(bool disabledBtns = false)
    {
        return new ComponentBuilder()
            .WithButton("Да", QuizConstants.LohComponentBtnYes, ButtonStyle.Success, disabled: disabledBtns)
            .WithButton("Нет", QuizConstants.LohComponentBtnNo, ButtonStyle.Danger, disabled: disabledBtns)
            .Build();
    }

    public static Embed LohComponentContent(string message, DateTime createdAt, DateTime finishedAt, bool isFinished = false)
    {
        string finishedText = isFinished ? "Закончен" :"Закончится";
        
        return new EmbedBuilder()
            .WithTitle("Внимание, лошки, опрос!")
            .WithDescription($"## {message}")
            .AddField("Начат", MessageUtils.GetCountdownText(createdAt))
            .AddField(finishedText, MessageUtils.GetCountdownText(finishedAt))
            .Build();
    }
}