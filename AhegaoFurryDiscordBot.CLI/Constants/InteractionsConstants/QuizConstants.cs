﻿namespace AhegaoFurryDiscordBot.CLI.Constants.InteractionsConstants;

public static class QuizConstants
{
    public const string QuizGroupName = "quiz";
    public const string QuizGroupDescription = "опросы";
    public const int SecondsTimeout = 60;
 
    public const string LohCommandName = "loh";
    public const string LohCommandDescription = "лох или нет";
    public const string LohComponentBtnYes = nameof(LohComponentBtnYes);
    public const string LohComponentBtnNo = nameof(LohComponentBtnNo);
}