﻿using AhegaoFurryDiscordBot.Infrastructure.Configurations;
using Discord.Interactions;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace AhegaoFurryDiscordBot.CLI.Builders;

public class BotBuilder
{
    private readonly HostApplicationBuilder _hostBuilder;
    private BotBuilder()
    {
        _hostBuilder = Host.CreateApplicationBuilder();
    }

    public IServiceCollection Services => _hostBuilder.Services;
    public IConfigurationManager Configuration => _hostBuilder.Configuration;
    
    public BotApplication Build()
    {
        var host = _hostBuilder.Build();

        using var scope = host.Services.CreateScope();

        var discordSocketClient = scope.ServiceProvider.GetRequiredService<DiscordSocketClient>();
        var interactionService = scope.ServiceProvider.GetRequiredService<InteractionService>();

        var botConfig = scope.ServiceProvider.GetRequiredService<IOptions<BotConfig>>();

        if (botConfig is null)
        {
            throw new ArgumentException(nameof(BotConfig) + " is null");
        }

        return new BotApplication(host, botConfig.Value, discordSocketClient, interactionService);
    }

    public static BotBuilder Create()
    {
        return new BotBuilder();
    }
}