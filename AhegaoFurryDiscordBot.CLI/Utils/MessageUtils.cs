﻿using System.Text.RegularExpressions;

namespace AhegaoFurryDiscordBot.CLI.Utils;

public static class MessageUtils
{
    public static bool IsUserPin(string tag)
    {
        return Regex.IsMatch(tag, "^<@[0-9]+>$");
    }

    public static string GetCountdownText(DateTime from)
    {
        long unixTimestamp = ((DateTimeOffset)from).ToUnixTimeSeconds();

        Console.WriteLine(unixTimestamp);
        
        return string.Format($"<t:{unixTimestamp}:R>");
    }
}